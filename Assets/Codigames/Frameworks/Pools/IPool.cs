﻿using System;

namespace Codigames.Frameworks.Pools
{
    public interface IPool<T> : IDisposable
    {
        T Spawn();
        void Despawn(T element);
    }
}
