using Sirenix.OdinInspector;
using UnityEngine;

namespace Codigames.Dinos.Zones
{
    public class ZoneView : MonoBehaviour
    {
        public string staticId;
        public string dynamicId;

        public string visualsPath;

        protected GameObject _loadedVisuals;

        public virtual void Initialize()
        {

        }

        public virtual void Dispose()
        {
            UnloadCurrentVisuals();
        }

        public virtual void LoadVisuals(int level)
        {
            UnloadCurrentVisuals();

            string levelSuffix = level.ToString("00");

            var visualsPrefab = Resources.Load<GameObject>("Codigames/Dinos/Zones/" + visualsPath + levelSuffix);
            _loadedVisuals = Instantiate(visualsPrefab, transform);

            OnVisualsLoaded();
        }

        public virtual void UnloadCurrentVisuals()
        {
            if (_loadedVisuals == null) return;

            Destroy(_loadedVisuals);
            _loadedVisuals = null;
        }

        protected virtual void OnVisualsLoaded() { }

#if UNITY_EDITOR
        [Button("Load first visual")]
        void LoadFirstVisualInEditor()
        {
            UnloadVisualsInEditor();

            var visualsPrefab = Resources.Load<GameObject>("Codigames/Dinos/Zones/" + visualsPath + "01");
            _loadedVisuals = Instantiate(visualsPrefab, transform);
        }

        [Button("Unload visual")]
        void UnloadVisualsInEditor()
        {
            if (_loadedVisuals == null) return;

            DestroyImmediate(_loadedVisuals);
            _loadedVisuals = null;
        }
#endif
    }
}