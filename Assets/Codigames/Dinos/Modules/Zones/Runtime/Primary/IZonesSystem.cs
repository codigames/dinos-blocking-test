using System.Collections.Generic;
using System;

namespace Codigames.Dinos.Zones
{
    public interface IZonesSystem
    {
        #region Fields
        void Initialize();
        void Dispose();

        void Tick(float deltaTime);

        ZoneController CreateZone(string staticId, string dynamicId);

        ZoneController Find(string zoneId);
        T FindFirst<T>(Predicate<T> filter) where T : ZoneController;
        List<T> FindAll<T>(Predicate<T> filter) where T : ZoneController;
        #endregion
    }
}