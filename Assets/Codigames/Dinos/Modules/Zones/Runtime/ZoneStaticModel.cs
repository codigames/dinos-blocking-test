using System.Collections.Generic;
using UnityEngine;

namespace Codigames.Dinos.Zones
{
    [CreateAssetMenu(menuName = "Dinos/Zones/ZoneStaticModel")]
    public class ZoneStaticModel : ScriptableObject
    {
        public string id;
        public string zoneType;

        public int maxLevel;
        public List<int> capacity;
    }
}