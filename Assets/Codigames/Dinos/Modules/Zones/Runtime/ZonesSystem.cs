using System.Collections.Generic;
using Zenject;

namespace Codigames.Dinos.Zones
{
    public class ZonesSystem : IZonesSystem
    {
        #region Fields
        [Inject] protected IZoneControllerFactory _controllerFactory;
        [Inject] protected IZoneStaticModelProvider _staticModelProvider;

        protected Dictionary<string, ZoneController> _zones;
        #endregion

        #region Methods
        public void Initialize()
        {
            _zones = new Dictionary<string, ZoneController>();
        }

        public void Dispose()
        {
            foreach (var zone in _zones.Values)
            {
                zone.Dispose();
            }

            _zones.Clear();
        }

        public void Tick(float deltaTime)
        {
            foreach (var zone in _zones.Values)
            {
                zone.Tick(deltaTime);
            }
        }

        public ZoneController Find(string zoneId)
        {
            _zones.TryGetValue(zoneId, out ZoneController zone);

            return zone;
        }

        public T FindFirst<T>(System.Predicate<T> filter) where T : ZoneController
        {
            foreach (var zone in _zones.Values)
            {
                if (zone is T tZone && filter.Invoke(tZone)) return tZone;
            }

            return null;
        }

        public List<T> FindAll<T>(System.Predicate<T> filter) where T : ZoneController
        {
            List<T> validZones = new List<T>();
            foreach (var zone in _zones.Values)
            {
                if (zone is T tZone && filter.Invoke(tZone)) validZones.Add(tZone);
            }

            return validZones;
        }

        public ZoneController CreateZone(string staticId, string dynamicId) 
        {
            ZoneStaticModel staticModel = _staticModelProvider.Find(staticId);
            ZoneController zone = _controllerFactory.Create(dynamicId, staticModel);

            zone.Initialize();
            _zones.Add(dynamicId, zone);

            return zone;
        }
        #endregion
    }
}