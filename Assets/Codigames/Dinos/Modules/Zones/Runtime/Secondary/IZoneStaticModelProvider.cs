
namespace Codigames.Dinos.Zones
{
    public interface IZoneStaticModelProvider
    {
        ZoneStaticModel Find(string staticId);
    }
}