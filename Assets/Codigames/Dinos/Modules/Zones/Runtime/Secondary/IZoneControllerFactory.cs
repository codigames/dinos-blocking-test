
namespace Codigames.Dinos.Zones
{
    public interface IZoneControllerFactory
    {
        ZoneController Create(string dynamicId, ZoneStaticModel staticModel);
    }
}