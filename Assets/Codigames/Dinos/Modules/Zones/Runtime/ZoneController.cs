
namespace Codigames.Dinos.Zones
{
    public class ZoneController
    {
        #region Fields
        protected int _level = 0;
        protected ZoneView _view;

        public readonly string dynamicId;
        public readonly ZoneStaticModel staticModel;
        #endregion

        #region Properties
        public int Level => _level;
        #endregion

        #region Constructors
        public ZoneController(string dynamicId, ZoneStaticModel staticModel)
        {
            this.dynamicId = dynamicId;
            this.staticModel = staticModel;
        }
        #endregion

        #region Methods
        public virtual void Initialize()
        {
            _level = 1;
        }

        public virtual void Dispose()
        {
            UnbindView();
        }

        public virtual void LevelUp()
        {
            if (_level == staticModel.maxLevel) return;

            _level++;
            _view.LoadVisuals(_level);

            RefreshZoneLogic();
        }

        public virtual void BindView(ZoneView view)
        {
            _view = view;
            _view.Initialize();
            _view.LoadVisuals(_level);

            RefreshZoneLogic();
        }

        public virtual void UnbindView()
        {
            _view.Dispose();
            _view = null;
        }

        public virtual void Tick(float deltaTime)
        {

        }

        public virtual void RefreshZoneLogic()
        {

        }
        #endregion
    }
}