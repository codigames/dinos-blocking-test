using UnityEngine;

namespace Codigames.Dinos.Game.Paths
{
    public class PathPoint : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        protected string _animationName;
        [SerializeField]
        protected Vector2 _waitTime;
        #endregion

        #region Properties
        public string AnimationName => _animationName;
        public float WaitTime => Random.Range(_waitTime.x, _waitTime.y);
        public Vector3 Position => transform.position;
        #endregion
    }
}
