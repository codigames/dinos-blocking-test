using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

namespace Codigames.Dinos.Game.Paths
{
    public class Path : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        protected List<PathPoint> _points;
        [Space]
        [SerializeField]
        protected bool _alwaysDrawGizmos;
        [SerializeField]
        protected Color _gizmoColor;
        #endregion

        #region Properties
        public int PointCount => _points.Count;
        #endregion

        #region Methods

        public PathPoint GetPoint(int index) => _points[index];

        [Button("Refresh Points")]
        void RefreshPoints()
        {
            _points = new List<PathPoint>(transform.GetComponentsInChildren<PathPoint>());
        }

        [Button("Reverse path")]
        void ReversePath()
        {
            _points.Reverse();
        }

        private void OnDrawGizmos()
        {
            if (_alwaysDrawGizmos) DrawPathGizmos();
        }

        private void OnDrawGizmosSelected()
        {
            if (!_alwaysDrawGizmos) DrawPathGizmos();
        }

        protected void DrawPathGizmos()
        {
            if (_points.Count < 3) return;

            Gizmos.color = _gizmoColor;

            DrawGizmoLine(_points[0], _points[_points.Count - 1]);

            for (int i = 1; i < _points.Count; i++)
            {
                DrawGizmoLine(_points[i], _points[i - 1]);
            }
        }
        protected void DrawGizmoLine(PathPoint a, PathPoint b)
        {
            Vector3 posA = a.Position;
            Vector3 posB = b.Position;

            Gizmos.DrawLine(posA, posB);
        }
        #endregion

    }
}