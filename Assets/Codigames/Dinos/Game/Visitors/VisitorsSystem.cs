using Codigames.Dinos.Game.Paths;
using Codigames.Dinos.Game.Units;
using Codigames.Frameworks.Pools;
using Codigames.Modules.CollectionUtils;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Codigames.Dinos.Game.Visitors
{
    public class VisitorsSystem : MonoBehaviour
    {
        [Inject] protected UnitsSystem _unitsSystem;

        [SerializeField]
        protected int _maxVisitors;
        [SerializeField]
        protected float _visitorsPerSecond;

        [SerializeField]
        protected List<Path> _visitorPaths;

        protected List<UnitController> _activeVisitors;

        protected float _spawnTimer;

        public void Initialize()
        {
            _activeVisitors = new List<UnitController>();
            _spawnTimer = 1f / _visitorsPerSecond;
        }

        public void Dispose()
        {
            _activeVisitors.Clear();
        }

        public void Tick(float deltaTime)
        {
            if (CanSpawnMoreVisitors())
            {
                _spawnTimer -= deltaTime;
                if (_spawnTimer <= 0f)
                {
                    SpawnVisitor();
                }
            }
        }

        bool CanSpawnMoreVisitors()
        {
            return _activeVisitors.Count < _maxVisitors;
        }

        void SpawnVisitor()
        {
            UnitController visitor = _unitsSystem.CreateUnit("Visitor");
            _activeVisitors.Add(visitor);

            visitor.Show();
            visitor.StartPath(_visitorPaths.GetRandom());

            _spawnTimer = 1f / _visitorsPerSecond;
        }
    }
}