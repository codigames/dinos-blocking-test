using Codigames.Dinos.Game.Select;
using Codigames.Dinos.Game.Units;
using Codigames.Dinos.Game.Visitors;
using Codigames.Dinos.Game.Zones;
using UnityEngine;
using Zenject;

namespace Codigames.Dinos.Game
{
    public class DinosGameInstaller : MonoInstaller
    {
        #region Fields
        [SerializeField]
        protected GameSystem _gameSystem;
        [SerializeField]
        protected UnitControllerFactory _unitControllerFactory;
        [SerializeField]
        protected VisitorsSystem _visitorsSystem;
        #endregion

        #region Methods
        public override void InstallBindings()
        {
            Container.BindInstance(_gameSystem).AsSingle();

            Container.Bind<UnitsSystem>().AsSingle();
            Container.BindInstance(_unitControllerFactory).AsSingle();

            Container.BindInstance(_visitorsSystem).AsSingle();

            SelectGameInstaller.Install(Container);
            ZonesGameInstaller.Install(Container);
        }
        #endregion
    }
}