using Zenject;

namespace Codigames.Dinos.Game.Select
{
    public class SelectGameInstaller : Installer<SelectGameInstaller>
    {
        #region Methods
        public override void InstallBindings()
        {
            Container.Bind<SelectSystem>().AsSingle();
        }
        #endregion
    }
}