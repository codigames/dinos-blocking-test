using Codigames.CameraManager.Primary;
using Codigames.Dinos.Game.UI.Popups;
using Codigames.Dinos.Game.UI.Zones;
using Codigames.Dinos.Game.Zones;
using Codigames.Dinos.Zones;
using HedgehogTeam.EasyTouch;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Codigames.Dinos.Game.Select
{
    public class SelectSystem
    {
        [Inject] protected MessagePopup _messagePopup;
        [Inject] protected ICameraService _cameraService;
        [Inject] protected IZonesSystem _zonesSystem;
        [Inject] protected DinoZoneMenu _dinoZoneMenu;

        public const string LAYER_MASK_NAME = "Gameplay";

        public void Initialize()
        {
            EasyTouch.On_SimpleTap += OnSimpleTap;
        }

        public void Dispose()
        {
            EasyTouch.On_SimpleTap += OnSimpleTap;
        }

        public void OnSceneObjectSelected(GameObject obj)
        {
            SelectCollider selectCollider = obj.GetComponent<SelectCollider>();

            if (selectCollider != null)
            {
                OnSelect(selectCollider);
                return;
            }

            ZoneView zoneView = obj.GetComponent<ZoneView>();
            if (zoneView != null)
            {
                OnZoneSelect(_zonesSystem.Find(zoneView.dynamicId));
            }
        }
        public void OnZoneSelect(ZoneController zone)
        {
            if (zone is DinoZoneController dinoZone)
            {
                _dinoZoneMenu.ShowMenu(dinoZone);
            }
        }

        public void OnSelect(SelectCollider selectCollider)
        {
            switch (selectCollider.selectId)
            {
                default:
                    ShowNotImplementedPopup(selectCollider);
                    break;
            }
        }

        protected void ShowNotImplementedPopup(SelectCollider selectCollider)
        {
            _messagePopup.ShowMessage("Element selected!", $"Tap on {selectCollider.selectId} not implemented");
        }

        private void OnSimpleTap(Gesture gesture)
        {
            if (EventSystem.current.IsPointerOverGameObject()) return;  // UI blocks this check

            Vector2 screenPosition = gesture.position;
            var layerMask = LayerMask.GetMask(LAYER_MASK_NAME);
            var ray = _cameraService.Camera.ScreenPointToRay(screenPosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask, QueryTriggerInteraction.Collide))
            {
                var sceneObject = hit.collider.gameObject;

                OnSceneObjectSelected(sceneObject);
            }
        }
    }
}