using Codigames.Dinos.Zones;
using Zenject;

namespace Codigames.Dinos.Game.Zones
{
    public class ZoneControllerFactory : IZoneControllerFactory
    {
        #region Fields
        [Inject] protected DiContainer _zenject;
        #endregion

        #region Methods
        public ZoneController Create(string dynamicId, ZoneStaticModel staticModel)
        {
            ZoneController zone;

            switch (staticModel.zoneType)
            {
                case "DinoZone":
                    zone = new DinoZoneController(dynamicId, staticModel);
                    break;
                default:
                    zone = new ZoneController(dynamicId, staticModel);
                    break;
            }

            _zenject.Inject(zone);

            return zone;
        }
        #endregion
    }
}