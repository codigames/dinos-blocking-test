using Codigames.Dinos.Zones;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Codigames.Dinos.Game.Zones
{
    public class ZoneStaticModelProvider : IZoneStaticModelProvider
    {
        public ZoneStaticModel Find(string staticId)
        {
            ZoneStaticModel staticModel = Resources.Load<ZoneStaticModel>("Codigames/Dinos/StaticModels/Zones/" + staticId);
            return staticModel;
        }
    }
}