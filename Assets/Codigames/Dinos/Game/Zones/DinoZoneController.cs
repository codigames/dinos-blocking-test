using Codigames.Dinos.Game.Units;
using Codigames.Dinos.Zones;
using Codigames.Modules.CollectionUtils;
using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Codigames.Dinos.Game.Zones
{
    public class DinoZoneController : ZoneController
    {
        #region Fields
        [Inject] protected UnitsSystem _unitsSystem;

        public List<UnitController> _dinos;
        #endregion

        #region Properties
        public DinoZoneView DinoZoneView => _view as DinoZoneView;
        public int Capacity => staticModel.capacity[Mathf.Min(_level - 1, staticModel.capacity.Count - 1)];
        public bool IsFull => Capacity <= _dinos.Count;
        #endregion

        #region Constructors
        public DinoZoneController(string dynamicId, ZoneStaticModel staticModel) : base(dynamicId, staticModel) { }
        #endregion

        #region Methods
        public override void Initialize()
        {
            base.Initialize();

            _dinos = new List<UnitController>();
        }
        public override void RefreshZoneLogic()
        {
            base.RefreshZoneLogic();

            if (DinoZoneView == null)
            {
                foreach (var dino in _dinos)
                {
                    dino.Hide();
                }
            }
            else
            {
                foreach (var dino in _dinos)
                {
                    ShowDinosaurInsideZone(dino);
                }
            }
        }

        public void AddDinosaur(UnitController dino)
        {
            _dinos.Add(dino);
            ShowDinosaurInsideZone(dino);
        }
        public void RemoveDinosaur(UnitController dino)
        {
            _dinos.Remove(dino);
            _unitsSystem.RemoveUnit(dino);
        }

        public List<UnitController> FindDinosaurs(Predicate<UnitController> filter)
        {
            List<UnitController> validDinos = new List<UnitController>();

            foreach (var dino in _dinos)
            {
                if (filter.Invoke(dino)) validDinos.Add(dino);
            }

            return validDinos;
        }

        protected void ShowDinosaurInsideZone(UnitController dino)
        {
            dino.Show();
            dino.StartPathOnRandomPoint(DinoZoneView.DinoPaths.GetRandom());
        }

        public void Reset()
        {
            _level = 0;

            List<UnitController> dinosToRemove = new List<UnitController>(_dinos);
            foreach (var dino in dinosToRemove)
            {
                RemoveDinosaur(dino);
            }

            LevelUp();
        }
        #endregion
    }
}