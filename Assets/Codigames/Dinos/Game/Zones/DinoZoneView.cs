using Codigames.Dinos.Game.Paths;
using Codigames.Dinos.Zones;
using System.Collections.Generic;

namespace Codigames.Dinos.Game.Zones
{
    public class DinoZoneView : ZoneView
    {
        public List<Path> DinoPaths { get; protected set; }

        protected override void OnVisualsLoaded()
        {
            base.OnVisualsLoaded();

            DinoPaths = new List<Path>(_loadedVisuals.transform.GetComponentsInChildren<Path>());
        }
    }
}