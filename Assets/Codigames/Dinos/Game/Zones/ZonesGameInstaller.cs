using Codigames.Dinos.Zones;
using Zenject;

namespace Codigames.Dinos.Game.Zones
{
    public class ZonesGameInstaller : Installer<ZonesGameInstaller>
    {
        #region Methods
        public override void InstallBindings()
        {
            Container.Bind<IZonesSystem>().To<ZonesSystem>().AsSingle();

            Container.Bind<IZoneControllerFactory>().To<ZoneControllerFactory>().AsSingle();
            Container.Bind<IZoneStaticModelProvider>().To<ZoneStaticModelProvider>().AsSingle();

            Container.Bind<ZoneSceneLoader>().AsSingle();
        }
        #endregion
    }
}