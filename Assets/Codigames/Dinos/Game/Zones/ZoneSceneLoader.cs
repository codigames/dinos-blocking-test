using Codigames.Dinos.Zones;
using Zenject;

namespace Codigames.Dinos.Game.Zones
{
    public class ZoneSceneLoader
    {
        #region Fields
        [Inject] protected IZonesSystem _zonesSystem;
        #endregion

        #region Methods
        public void LoadZonesInScene()
        {
            var zoneViews = UnityEngine.Object.FindObjectsOfType<ZoneView>();

            foreach (var view in zoneViews)
            {
                ZoneController zone = _zonesSystem.CreateZone(view.staticId, view.dynamicId);
                zone.BindView(view);
            }
        }
        #endregion
    }
}