using Codigames.Frameworks.Pools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Codigames.Dinos.Game.Units
{
    public class UnitsSystem
    {
        [Inject] protected UnitControllerFactory _unitFactory;

        public Dictionary<int, UnitController> _activeUnits;
        protected List<int> _unitsToRemove;

        protected int _idCount;

        public void Initialize()
        {
            _unitFactory.Initialize();
            _activeUnits = new Dictionary<int, UnitController>();
            _unitsToRemove = new List<int>();
            _idCount = 0;
        }

        public void Dispose()
        {
            _unitFactory.Dispose();
        }

        public void Tick(float deltaTime)
        {
            foreach (var id in _unitsToRemove)
            {
                _activeUnits.Remove(id);
            }
            _unitsToRemove.Clear();

            foreach (var unit in _activeUnits.Values)
            {
                unit.Tick(deltaTime);
            }
        }

        public void InitializeSceneUnits()
        {
            foreach(var unit in GameObject.FindObjectsOfType<UnitController>())
            {
                unit.Initialize();

                int id = GenerateUniqueId();
                unit.SetId(id);
                _activeUnits.Add(id, unit);

                if (unit.HasPredefinedPath())
                {
                    unit.Show();
                    unit.StartPath(unit.PredefinedPath);
                }
            }
        }

        public UnitController CreateUnit(string poolId)
        {
            UnitController unit = _unitFactory.Spawn(poolId);

            unit.Initialize();

            int id = GenerateUniqueId();
            unit.SetId(id);
            _activeUnits.Add(id, unit);

            return unit;
        }

        public void RemoveUnit(UnitController unit)
        {
            _unitsToRemove.Add(unit.Id);
            _unitFactory.Despawn(unit);
        }

        protected int GenerateUniqueId()
        {
            _idCount++;
            return _idCount;
        }
    }
}