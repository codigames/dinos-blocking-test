using Codigames.Frameworks.Pools;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Codigames.Dinos.Game.Units
{
    public class UnitControllerFactory : MonoBehaviour
    {
        [Inject] protected DiContainer _zenject;

        [SerializeField]
        protected Transform _masterContainer;
        [SerializeField]
        public List<PairStringUnitController> _unitPrefabs;

        protected Dictionary<string, InternalPool<UnitController>> _unitPools;

        public void Initialize()
        {
            _unitPools = new Dictionary<string, InternalPool<UnitController>>();

            GeneratePools();
        }

        public void Dispose()
        {
            foreach (var pool in _unitPools.Values)
            {
                pool.Dispose();
            }

            _unitPools.Clear();
        }

        public UnitController Spawn(string id)
        {
            UnitController unit = null;

            bool success = _unitPools.TryGetValue(id, out InternalPool<UnitController> pool);

            if (success)
            {
                unit = pool.Spawn();
                unit.SetPoolId(id);
            }

            return unit;
        }

        public void Despawn(UnitController unit)
        {
            bool success = _unitPools.TryGetValue(unit.PoolId, out InternalPool<UnitController> pool);

            if (success)
            {
                pool.Despawn(unit);
            }
        }

        protected void GeneratePools()
        {
            foreach (var pair in _unitPrefabs)
            {
                GeneratePool(pair.id, pair.prefab);
            }
        }

        protected void GeneratePool(string id, UnitController prefab)
        {
            string poolName = "UnitPool_" + id;

            Transform container = _masterContainer.Find(poolName);
            if (container == null)
            {
                container = new GameObject(poolName).transform;
                container.SetParent(_masterContainer);
            }

            InternalPool<UnitController> pool = new InternalPool<UnitController>(prefab, container, 3, _zenject);
            _unitPools.Add(id, pool);
        }
    }
}