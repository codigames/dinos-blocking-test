using Sirenix.OdinInspector;
using UnityEngine;

namespace Codigames.Dinos.Game.Units
{
    public class DinoUnitController : UnitController
    {
        [Space]
        [SerializeField]
        protected Vector2 _sizeVariation;
        [SerializeField]
        protected Vector2 _speedVariation;
        [SerializeField]
        protected Vector2 _animationSpeedVariation;

        [Space]
        [SerializeField]
        protected bool _forceAge;

        [ShowIf(nameof(_forceAge))]
        [SerializeField, Range(0,1)]
        protected float _forcedValue;

        public override void Initialize()
        {
            float age = _forceAge ? _forcedValue : Random.value;
            _movementSpeed = Mathf.Lerp(_speedVariation.x, _speedVariation.y, age);
            transform.localScale = Vector3.one * Mathf.Lerp(_sizeVariation.x, _sizeVariation.y, age);
            _animator.speed = Mathf.Lerp(_animationSpeedVariation.x, _animationSpeedVariation.y, age);

            base.Initialize();
        }
    }
}