using Codigames.Dinos.Game.Paths;
using DG.Tweening;
using UnityEngine;

namespace Codigames.Dinos.Game.Units
{
    public class UnitController : MonoBehaviour
    {
        #region Fields
        public string type;

        [Space]
        [SerializeField]
        protected float _movementSpeed;
        [SerializeField]
        protected float _rotationSpeed;

        [Space]
        [SerializeField]
        protected float _maxPointOffset;

        [Space]
        [SerializeField]
        protected Animator _animator;
        [SerializeField]
        protected Path _predefinedPath;

        protected Path _activePath;

        protected Sequence _moveSequence;

        protected int _pathIndex;
        #endregion

        #region Properties
        public string PoolId { get; protected set; }
        public int Id { get; protected set; }
        public Path PredefinedPath => _predefinedPath;
        #endregion

        #region Methods
        public virtual void Initialize()
        {
            Hide();
        }

        public virtual void Dispose()
        {
            _moveSequence?.Kill();
            _activePath = null;
        }

        public virtual void Tick(float deltaTime)
        {

        }
        public void SetId(int id)
        {
            Id = id;
        }

        public void SetPoolId(string poolId)
        {
            PoolId = poolId;
        }

        public bool HasPredefinedPath()
        {
            return _predefinedPath != null;
        }

        public virtual void StartPath(Path path)
        {
            _activePath = path;
            StartActivePath(0);
        }

        public virtual void StartPathOnRandomPoint(Path path)
        {
            _activePath = path;
            StartActivePath(Random.Range(0, path.PointCount));
        }

        protected void StartActivePath(int index)
        {
            _pathIndex = index;
            transform.position = _activePath.GetPoint(_pathIndex).Position;

            MoveToNextPathPoint();
        }

        public void StopPath()
        {
            _moveSequence?.Kill();
            SetAnimation("idle");
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }


        protected void MoveToNextPathPoint()
        {
            PathPoint point = GetNextPoint();

            _moveSequence?.Kill();
            _moveSequence = DOTween.Sequence();

            _moveSequence.AppendCallback(() => SetAnimation("moving"));

            Vector3 position = point.Position + UnityEngine.Random.insideUnitSphere * _maxPointOffset;
            position.y = point.Position.y;

            Vector3 direction = point.Position - transform.position;

            float angle = Vector3.Angle(transform.forward, direction);
            float rotateTime = angle / _rotationSpeed;
            _moveSequence.Append(transform.DOLookAt(position, rotateTime).SetEase(Ease.Linear));

            float dist = direction.magnitude;
            float moveTime = dist / _movementSpeed;
            _moveSequence.Join(transform.DOMove(position, moveTime).SetEase(Ease.Linear));

            float waitTime = point.WaitTime;
            if (waitTime > 0f)
            {
                _moveSequence.AppendCallback(() => SetAnimation(point.AnimationName));
                _moveSequence.AppendInterval(waitTime);
            }

            _moveSequence.OnComplete(() => MoveToNextPathPoint());

            _moveSequence.Play();
        }

        public void SetAnimation(string stateName)
        {
            if (_animator == null) return;
            if (string.IsNullOrEmpty(stateName)) stateName = "idle";

            _animator.Play(stateName, 0, 0f);
        }

        protected PathPoint GetNextPoint()
        {
            if (_pathIndex >= _activePath.PointCount - 1)
            {
                _pathIndex = 0;
            }
            else
            {
                _pathIndex++;
            }

            return _activePath.GetPoint(_pathIndex);
        }
        #endregion
    }
}