
namespace Codigames.Dinos.Game.Units
{
    [System.Serializable]
    public class PairStringUnitController
    {
        public string id;
        public UnitController prefab;
    }
}