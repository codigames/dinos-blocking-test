using Codigames.Modules.CollectionUtils;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Codigames.Dinos.Game.Units
{
    public class UnitsScreenshotHelper : MonoBehaviour
    {
        [Inject] protected UnitsSystem _unitsSystem;

        [SerializeField]
        protected List<string> _unitIds;
        [SerializeField]
        protected List<string> _animations;

        [SerializeField]
        protected Vector2Int _matrix;
        [SerializeField]
        protected float _distance;

        protected UnitController[,] _units;

        [ContextMenu("Set up")]
        public void SetUp()
        {
            if (_units == null || _units.GetLength(0) == 0)
            {
                FirstSetUp();
                return;
            }

            for(int x = 0; x < _matrix.x; x++)
            {
                for(int y = 0; y < _matrix.y; y++)
                {
                    var unit = _units[x, y];
                    SetUpUnit(unit);
                }
            }
        }

        protected void FirstSetUp()
        {
            _units = new UnitController[_matrix.x, _matrix.y];

            for (int x = 0; x < _matrix.x; x++)
            {
                for (int y = 0; y < _matrix.y; y++)
                {
                    var unit = _unitsSystem.CreateUnit(_unitIds.GetRandom());
                    unit.transform.position = new Vector3(x * _distance, 0, y * _distance);

                    SetUpUnit(unit);
                    _units[x, y] = unit;
                }
            }
        }

        protected void SetUpUnit(UnitController unit)
        {
            unit.transform.eulerAngles = new Vector3(0, Random.Range(0, 359), 0);
            unit.Show();
            unit.SetAnimation(_animations.GetRandom());
        }
    }
}