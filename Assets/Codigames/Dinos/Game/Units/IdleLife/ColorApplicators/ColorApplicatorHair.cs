﻿using UnityEngine;

namespace Codigames.Dinos.Game.Units.IdleLife.ColorApplicators
{
    public class ColorApplicatorHair : ColorApplicatorBase
    {
        public ColorApplicatorHair() : base()
        {
        }

        public override bool ApplyColor(Material skinMaterial, Material hairMaterial, Material eyeMaterial, Material faceMaterial, Color color, int offsetIndex)
        {
            return SetMaterialOffset(hairMaterial, offsetIndex);
        }
    }
}
