﻿using UnityEngine;

namespace Codigames.Dinos.Game.Units.IdleLife.ColorApplicators
{
    public class ColorApplicatorEyes : ColorApplicatorBase
    {
        public ColorApplicatorEyes() : base()
        {
        }

        public override bool ApplyColor(Material skinMaterial, Material hairMaterial, Material eyeMaterial, Material faceMaterial, Color color, int offsetIndex)
        {
            eyeMaterial.SetColor(UnitCustomizationController.MAT_COLOR_ID, color);
            return true;
        }
    }
}
