﻿using UnityEngine;

namespace Codigames.Dinos.Game.Units.IdleLife.ColorApplicators
{
    public class ColorApplicatorBase
    {
        public ColorApplicatorBase()
        {
        }

        public virtual bool ApplyColor(Material skinMaterial, Material hairMaterial, Material eyeMaterial, Material faceMaterial, Color color, int offsetIndex)
        {
            return false;
        }
        protected bool SetMaterialOffset(Material material, int offsetIndex)
        {
            Vector2 offset = material.mainTextureOffset;
            offset.y = 1 - (offsetIndex / 255f);
            material.mainTextureOffset = offset;

            return true;
        }
    }
}
