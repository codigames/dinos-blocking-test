﻿using UnityEngine;

namespace Codigames.Dinos.Game.Units.IdleLife.ColorApplicators
{
    public class ColorApplicatorSkin : ColorApplicatorBase
    {
        public ColorApplicatorSkin() : base()
        {
        }

        public override bool ApplyColor(Material skinMaterial, Material hairMaterial, Material eyeMaterial, Material faceMaterial, Color color, int offsetIndex)
        {
            SetMaterialOffset(skinMaterial, offsetIndex);
            faceMaterial.SetColor(UnitCustomizationController.MAT_COLOR_ID, color);
            return true;
        }
    }
}
