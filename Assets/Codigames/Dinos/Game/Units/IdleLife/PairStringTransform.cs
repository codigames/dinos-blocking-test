﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Codigames.Dinos.Game.Units.IdleLife.Utils
{
    [System.Serializable]
    public class PairStringTransform
    {
        public string Key;
        public Transform Value;
    }

    public static class PairStringTransformExtensions
    {
        public static Dictionary<string, Transform> ToDictionary(this List<PairStringTransform> pairList)
        {
            Dictionary<string, Transform> dictionary = new Dictionary<string, Transform>();

            foreach (var pair in pairList)
            {
                dictionary.Add(pair.Key, pair.Value);
            }

            return dictionary;
        }
    }
}