﻿using Codigames.Dinos.Game.Units.IdleLife.ColorApplicators;
using Codigames.Dinos.Game.Units.IdleLife.Utils;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

namespace Codigames.Dinos.Game.Units.IdleLife
{
    public class UnitCustomizationController : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        protected Transform _rigRoot;
        [SerializeField]
        protected Transform _faceContainer;
        [SerializeField]
        protected Transform _hairContainer;

        [Space]
        [SerializeField]
        protected FaceViewData _staticFaceViewData;
        [SerializeField]
        protected SkinnedMeshRenderer[] _staticRenderers;

        // ------------

        [TabGroup("Colors"), SerializeField]
        protected int _eyeColorCount;
        [TabGroup("Colors"), SerializeField]
        protected int _skinColorCount;
        [TabGroup("Colors"), SerializeField]
        protected int _hairColorCount;

        [Space]
        [TabGroup("Colors"), SerializeField]
        protected List<Color> eyeColors;
        [TabGroup("Colors"), SerializeField]
        protected List<Color> skinDetailColors;
        [TabGroup("Colors"), SerializeField]
        protected List<Color> hairDetailColors;

        // ------------

        [TabGroup("Parts"), SerializeField]
        protected int _bodyCount;
        [TabGroup("Parts"), SerializeField]
        protected int _legsCount;
        [TabGroup("Parts"), SerializeField]
        protected int _feetCount;

        [Space]
        [TabGroup("Parts"), SerializeField]
        protected string _bodyPath;
        [TabGroup("Parts"), SerializeField]
        protected string _legsPath;
        [TabGroup("Parts"), SerializeField]
        protected string _feetPath;

        // ------------

        [TabGroup("Head"), SerializeField]
        protected int _faceCount;
        [TabGroup("Head"), SerializeField]
        protected int _hairCount;

        [Space]
        [TabGroup("Head"), SerializeField]
        protected string _facePath;
        [TabGroup("Head"), SerializeField]
        protected string _hairPath;

        // -------------

        [TabGroup("Materials"), SerializeField]
        protected Material _skinMaterial;
        [TabGroup("Materials"), SerializeField]
        protected Material _hairMaterial;

        [TabGroup("Materials"), SerializeField, Space(10)]
        protected Material _faceMaterial;
        [TabGroup("Materials"), SerializeField]
        protected Material _eyeMaterial;

        [TabGroup("Materials"), SerializeField, Space(10)]
        protected Material _clothMaterial;
        [TabGroup("Materials"), SerializeField]
        protected Material _glassMaterial;

        // -------------

        [TabGroup("Renderers"), SerializeField]
        protected List<PairStringSkinnedMeshRenderer> _meshRenderers;

        // -------------

        protected Material _skinMatInstance;
        protected Material _hairMatInstance;
        protected Material _eyeMatInstance;

        protected Dictionary<string, SkinnedMeshRenderer> _rendererDictionary = new Dictionary<string, SkinnedMeshRenderer>();
        protected Dictionary<string, Transform> _boneDictionary = new Dictionary<string, Transform>();
        protected Dictionary<string, Material> _materialsDictionary = new Dictionary<string, Material>();

        protected FaceViewData _currentFace;

        protected Dictionary<string, ColorApplicatorBase> _colorApplicators = new Dictionary<string, ColorApplicatorBase> {
            { "EYES", new ColorApplicatorEyes() },
            { "HAIR", new ColorApplicatorHair() },
            { "SKIN", new ColorApplicatorSkin() }
        };

        public const string MAT_COLOR_ID = "_Color";
        #endregion

        #region Methods
        public void Initialize()
        {
            SetMaterials();

            _rendererDictionary = _meshRenderers.ToDictionary();

            SetBoneDictionary();
        }

        public void SetRandomColors()
        {
            ReplaceRendererMaterials(_staticRenderers);

            int eyeColor = Random.Range(0, _eyeColorCount);
            int skinColor = Random.Range(0, _skinColorCount);
            int hairColor = Random.Range(0, _hairColorCount);

            ApplyColor("EYES", eyeColors[eyeColor], eyeColor);
            ApplyColor("SKIN", skinDetailColors[skinColor], skinColor);
            ApplyColor("HAIR", hairDetailColors[hairColor], hairColor);
        }

        public void SetRandomOutfit()
        {
            LoadFace(Random.Range(0, _faceCount), _facePath);
            LoadHair(Random.Range(0, _hairCount), _hairPath);

            LoadBodyPart(Random.Range(0, _bodyCount), _bodyPath);
            LoadBodyPart(Random.Range(0, _legsCount), _legsPath);
            LoadBodyPart(Random.Range(0, _feetCount), _feetPath);
        }


        public void Dispose()
        {
            _skinMatInstance = null;
            _hairMatInstance = null;

            Destroy(_currentFace);

            _materialsDictionary.Clear();

            _rendererDictionary.Clear();
            _boneDictionary.Clear();
        }

        public bool ApplyColor(string colorPart, Color color, int offsetIndex)
        {
            if (_colorApplicators.ContainsKey(colorPart) == false)
            {
                Debug.LogError("ERROR: Color applicator not available for part " + colorPart.ToString());
                return false;
            }

            return _colorApplicators[colorPart].ApplyColor(_skinMatInstance, _hairMatInstance, _eyeMatInstance, _materialsDictionary[_faceMaterial.name], color, offsetIndex);
        }

        public Transform GetVisualTransform()
        {
            return transform;
        }

        protected void LoadFace(int index, string path)
        {
            string suffix = (index + 1).ToString("00");

            FaceViewData face = Resources.Load<FaceViewData>(path + suffix);

            SetFace(face);
        }

        protected void SetFace(FaceViewData face)
        {
            FaceViewData instance = Instantiate(face, _faceContainer);

            ReplaceRendererMaterials(instance.Renderers);
            ReplaceRendererMaterials(_staticFaceViewData.Renderers);
        }

        protected void LoadHair(int index, string path)
        {
            string suffix = (index + 1).ToString("00");

            HairViewData face = Resources.Load<HairViewData>(path + suffix);

            SetHair(face);
        }

        protected void SetHair(HairViewData hair)
        {
            HairViewData instance = Instantiate(hair, _hairContainer);

            ReplaceRendererMaterials(instance.Renderers);
        }

        protected void LoadBodyPart(int index, string path)
        {
            string suffix = (index + 1).ToString("00");

            BodyPartViewData part = Resources.Load<BodyPartViewData>(path + suffix);

            SetBodyPart(part);
        }

        protected void SetBodyPart(BodyPartViewData part)
        {
            if (_rendererDictionary.ContainsKey(part.rendererId))
            {
                SkinnedMeshRenderer rend = _rendererDictionary[part.rendererId];
                rend.sharedMesh = part.GetMesh();

                RefreshRendererBones(rend, _boneDictionary, part.GetBones());
                CustomizationUtils.ReplaceMaterialsByName(rend, part.GetMaterials(), _materialsDictionary);
            }
        }

        protected void ReplaceRendererMaterials(Renderer[] rends)
        {
            foreach (Renderer rend in rends)
            {
                CustomizationUtils.ReplaceMaterialsByName(rend, rend.sharedMaterials, _materialsDictionary);
            }
        }

        protected void SetBoneDictionary()
        {
            _boneDictionary.Clear();

            Transform[] children = _rigRoot.GetComponentsInChildren<Transform>(true);

            foreach (Transform child in children)
            {
                _boneDictionary.Add(child.name, child);
            }
        }

        protected void RefreshRendererBones(SkinnedMeshRenderer rend, Dictionary<string, Transform> viewBones, Transform[] newBones)
        {
            Transform[] bones = new Transform[newBones.Length];

            for (int boneOrder = 0; boneOrder < bones.Length; boneOrder++)
            {
                string name = newBones[boneOrder].name;
                if (viewBones.ContainsKey(name))
                {
                    bones[boneOrder] = viewBones[name];
                }
            }
            rend.bones = bones;
        }

        protected void SetMaterials()
        {
            _eyeMatInstance = new Material(_eyeMaterial)
            {
                name = _eyeMaterial.name
            };
            Material faceMat = new Material(_faceMaterial)
            {
                name = _faceMaterial.name
            };

            _hairMatInstance = new Material(_hairMaterial)
            {
                name = _hairMaterial.name
            };
            _skinMatInstance = new Material(_skinMaterial)
            {
                name = _skinMaterial.name
            };

            AddMaterialToDictionary(_glassMaterial);
            AddMaterialToDictionary(faceMat);
            AddMaterialToDictionary(_clothMaterial);

            AddMaterialToDictionary(_hairMatInstance);
            AddMaterialToDictionary(_skinMatInstance);

            AddMaterialToDictionary(_eyeMatInstance);
        }

        protected void AddMaterialToDictionary(Material mat)
        {
            if (!_materialsDictionary.ContainsKey(mat.name))
            {
                _materialsDictionary.Add(mat.name, mat);
            }
        }
        #endregion
    }
}