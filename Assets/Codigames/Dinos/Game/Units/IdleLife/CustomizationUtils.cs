﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Codigames.Dinos.Game.Units.IdleLife.Utils
{
    public class CustomizationUtils
    {
        public static void ReplaceMaterialsByName(Renderer rend, Material[] oldMaterials, Dictionary<string, Material> materials)
        {
            Material[] mats = new Material[oldMaterials.Length];

            int count = 0;
            foreach (var mat in oldMaterials)
            {
                if (mat != null)
                {
                    if (materials.ContainsKey(mat.name))
                    {
                        mats[count] = materials[mat.name];
                    }
                    else
                    {
                        mats[count] = mat;
                    }
                }
                else
                {
                    mats[count] = null;
                }

                count++;
            }

            rend.sharedMaterials = mats;
        }
    }
}