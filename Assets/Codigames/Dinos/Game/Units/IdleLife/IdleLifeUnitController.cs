using UnityEngine;

namespace Codigames.Dinos.Game.Units.IdleLife
{
    public class IdleLifeUnitController : UnitController
    {
        [Space]
        [SerializeField]
        protected UnitCustomizationController _customization;

        public override void Initialize()
        {
            base.Initialize();

            _customization.Initialize();

            _customization.SetRandomOutfit();
            _customization.SetRandomColors();
        }

        public override void Dispose()
        {
            base.Dispose();

            _customization.Dispose();
        }
    }
}
