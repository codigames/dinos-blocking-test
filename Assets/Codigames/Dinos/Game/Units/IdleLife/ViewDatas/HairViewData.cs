using UnityEngine;

namespace Codigames.Dinos.Game.Units.IdleLife
{
    public class HairViewData : MonoBehaviour
    {
        #region Fields
        public Renderer[] Renderers => GetComponentsInChildren<Renderer>(true);
        #endregion
    }
}