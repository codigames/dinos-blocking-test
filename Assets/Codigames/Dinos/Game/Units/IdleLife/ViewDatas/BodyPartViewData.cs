﻿using UnityEngine;

namespace Codigames.Dinos.Game.Units.IdleLife
{
    public class BodyPartViewData : MonoBehaviour
    {
        public string rendererId;

        protected SkinnedMeshRenderer _renderer;

        public Transform[] GetBones()
        {
            if (_renderer == null) _renderer = GetComponentInChildren<SkinnedMeshRenderer>(true);
            return _renderer.bones;
        }

        public Material[] GetMaterials()
        {
            if (_renderer == null) _renderer = GetComponentInChildren<SkinnedMeshRenderer>(true);
            return _renderer.sharedMaterials;
        }

        public Mesh GetMesh()
        {
            if (_renderer == null) _renderer = GetComponentInChildren<SkinnedMeshRenderer>(true);
            return _renderer.sharedMesh;
        }

        public Transform GetVisualTransform()
        {
            if (_renderer == null) _renderer = GetComponentInChildren<SkinnedMeshRenderer>(true);
            return _renderer.transform;
        }
    }
}