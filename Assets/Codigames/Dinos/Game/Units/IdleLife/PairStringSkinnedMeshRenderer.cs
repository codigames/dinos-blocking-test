﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Codigames.Dinos.Game.Units.IdleLife.Utils
{
    [System.Serializable]
    public class PairStringSkinnedMeshRenderer
    {
        public string Key;
        public SkinnedMeshRenderer Value;
    }

    public static class PairStringSkinnedMeshRendererExtensions
    {
        public static Dictionary<string, SkinnedMeshRenderer> ToDictionary(this List<PairStringSkinnedMeshRenderer> pairList)
        {
            Dictionary<string, SkinnedMeshRenderer> dictionary = new Dictionary<string, SkinnedMeshRenderer>();

            foreach(var pair in pairList)
            {
                dictionary.Add(pair.Key, pair.Value);
            }

            return dictionary;
        }
    }
}