using Codigames.CameraBounds3d;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Codigames.Dinos.Game.UI.ZoomTests
{
    public class ZoomSettingsMenu : MenuBase
    {
        #region Fields
        [SerializeField]
        protected CameraBounds3dZoom _camZoom;

        [Space]
        [SerializeField]
        protected ZoomElementWidget _maxZoomWidget;
        [SerializeField]
        protected ZoomElementWidget _minZoomWidget;
        [SerializeField]
        protected ZoomElementWidget _pinchFactorWidget;

        [Space]
        [SerializeField]
        protected ZoomPresetSettings _zoomPresetSettings;
        [SerializeField]
        protected List<Button> _zoomPresetButtons;

        protected bool _isInitialized;
        #endregion

        #region Methods
        public override void Initialize()
        {
            if (_isInitialized) return;

            _maxZoomWidget.Initialize(2f, 12f, 0.5f, _camZoom.maximumZoom, OnMaxZoomChanged);
            _minZoomWidget.Initialize(0.25f, 4f, 0.25f, _camZoom.minimumZoom, OnMinZoomChanged);
            _pinchFactorWidget.Initialize(500f, 10000f, 250f, _camZoom.ReducePinchFactor, OnPinchFactorChanged);

            foreach (var button in _zoomPresetButtons)
            {
                button.onClick.RemoveAllListeners();
            }

            for(int i = 0; i < Mathf.Min(_zoomPresetSettings.presets.Count, _zoomPresetButtons.Count); i++)
            {
                ZoomPreset preset = _zoomPresetSettings.presets[i];
                Button button = _zoomPresetButtons[i];

                button.onClick.AddListener(() => OnZoomPresetSelected(preset));
            }

            base.Initialize();

            _isInitialized = true;
        }

        public override void Dispose()
        {
            if (!_isInitialized) return;

            _maxZoomWidget.Dispose();
            _minZoomWidget.Dispose();
            _pinchFactorWidget.Dispose();

            _isInitialized = false;
        }

        public void Tick(float deltaTime)
        {
            if (!_isInitialized) return;
        }

        void OnMinZoomChanged(float minZoom)
        {
            _camZoom.minimumZoom = minZoom;
        }

        void OnMaxZoomChanged(float maxZoom)
        {
            _camZoom.maximumZoom = maxZoom;
        }

        void OnPinchFactorChanged(float pinchFactor)
        {
            _camZoom.ReducePinchFactor = pinchFactor;
        }

        void OnZoomPresetSelected(ZoomPreset preset)
        {
            OnMinZoomChanged(preset.minZoom);
            _minZoomWidget.ForceValue(preset.minZoom);

            OnMaxZoomChanged(preset.maxZoom);
            _maxZoomWidget.ForceValue(preset.maxZoom);

            OnPinchFactorChanged(preset.reducePinchFactor);
            _pinchFactorWidget.ForceValue(preset.reducePinchFactor);
        }
        #endregion
    }
}