
namespace Codigames.Dinos.Game.UI.ZoomTests
{
    [System.Serializable]
    public class ZoomPreset
    {
        public float minZoom;
        public float maxZoom;
        public float reducePinchFactor;
        public float currentZoom;
    }
}