﻿
using Codigames.CameraBounds3d;
using UnityEngine;

namespace Codigames.Dinos.Game.UI.ZoomTests
{
    public class UIZoomHelper : MonoBehaviour
    {
        [SerializeField] ZoomPresetSettings _settings;
        [SerializeField] CameraBounds3dZoom _cameraZoom;

        public void ApplyPreset(int number)
        {
            if (number < 0 && number >= _settings.presets.Count) return;

            var zoomPreset = _settings.presets[number];
            
            _cameraZoom.minimumZoom = zoomPreset.minZoom;
            _cameraZoom.maximumZoom = zoomPreset.maxZoom;
            _cameraZoom.ReducePinchFactor = zoomPreset.reducePinchFactor;
            _cameraZoom.ApplyZoomTween(zoomPreset.currentZoom);
        }
    }
}
