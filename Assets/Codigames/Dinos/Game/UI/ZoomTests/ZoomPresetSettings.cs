using System.Collections.Generic;
using UnityEngine;

namespace Codigames.Dinos.Game.UI.ZoomTests
{
    [CreateAssetMenu(menuName = "Dinos/ZoomPresetSettings")]
    [System.Serializable]
    public class ZoomPresetSettings : ScriptableObject
    {
        public List<ZoomPreset> presets;
    }
}