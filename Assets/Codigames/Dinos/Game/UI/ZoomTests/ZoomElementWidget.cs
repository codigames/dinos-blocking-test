using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Codigames.Dinos.Game.UI.ZoomTests
{
    public class ZoomElementWidget : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        protected Button _valueUpButton;
        [SerializeField]
        protected Button _valueDownButton;
        [SerializeField]
        protected TextMeshProUGUI _valueLabel;

        protected float _currentValue;
        protected float _minValue;
        protected float _maxValue;
        protected float _increment;

        System.Action<float> _onValueChanged;
        #endregion

        #region Methods
        public void Initialize(float min, float max, float increment, float current, System.Action<float> onValueChanged)
        {
            _minValue = min;
            _maxValue = max;
            _increment = increment;
            _currentValue = current;
            _valueLabel.text = _currentValue.ToString("0.##");

            _onValueChanged = onValueChanged;

            _valueUpButton.onClick.RemoveAllListeners();
            _valueUpButton.onClick.AddListener(ValueUp);

            _valueDownButton.onClick.RemoveAllListeners();
            _valueDownButton.onClick.AddListener(ValueDown);
        }

        public void Dispose()
        {
            _onValueChanged = null;
        }

        public void ValueUp()
        {
            SetNewValue(_currentValue + _increment);
        }

        public void ValueDown()
        {
            SetNewValue(_currentValue - _increment);
        }

        protected void SetNewValue(float newValue)
        {
            _currentValue = Mathf.Clamp(newValue, _minValue, _maxValue);
            _valueLabel.text = _currentValue.ToString("0.##");

            _onValueChanged?.Invoke(_currentValue);
        }

        public void ForceValue(float newValue)
        {
            _currentValue = newValue;
            _valueLabel.text = _currentValue.ToString("0.##");
        }
        #endregion
    }
}