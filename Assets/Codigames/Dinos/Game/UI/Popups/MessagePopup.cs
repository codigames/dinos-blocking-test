using TMPro;
using UnityEngine;

namespace Codigames.Dinos.Game.UI.Popups
{
    public class MessagePopup : MenuBase
    {
        [SerializeField]
        protected TextMeshProUGUI _titleLabel;
        [SerializeField]
        protected TextMeshProUGUI _subtitleLabel;

        public void ShowMessage(string title, string subtitle)
        {
            _titleLabel.text = title;
            _subtitleLabel.text = subtitle;

            ShowMenu();
        }
    }
}