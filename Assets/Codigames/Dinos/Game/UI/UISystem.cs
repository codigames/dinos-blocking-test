using Codigames.Dinos.Game.UI.Popups;
using Codigames.Dinos.Game.UI.Zones;
using Codigames.Dinos.Game.UI.ZoomTests;
using Zenject;

namespace Codigames.Dinos.Game.UI
{
    public class UISystem
    {
        #region Fields
        [Inject] protected ZoomSettingsMenu _zoomSettingsMenu;
        [Inject] protected MessagePopup _messagePopup;
        [Inject] protected DinoZoneMenu _dinoZoneMenu;
        #endregion

        #region Methods
        public void Initialize()
        {
            _zoomSettingsMenu.Initialize();
            _messagePopup.Initialize();
            _dinoZoneMenu.Initialize();
        }

        public void Dispose()
        {
            _zoomSettingsMenu.Dispose();
            _messagePopup.Dispose();
            _dinoZoneMenu.Dispose();
        }

        public void Tick(float deltaTime)
        {

        }
        #endregion
    }
}