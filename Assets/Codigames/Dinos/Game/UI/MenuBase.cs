using UnityEngine;
using UnityEngine.UI;

namespace Codigames.Dinos.Game.UI
{
    public class MenuBase : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        protected Button _closeButton;
        #endregion

        #region Methods
        public virtual void Initialize()
        {
            if (_closeButton != null)
            {
                _closeButton.onClick.RemoveAllListeners();
                _closeButton.onClick.AddListener(OnCloseButtonClick);
            }

            HideMenu();
        }

        public virtual void Dispose() { }

        protected virtual void OnCloseButtonClick()
        {
            HideMenu();
        }

        public virtual void ShowMenu()
        {
            gameObject.SetActive(true);
        }

        public virtual void HideMenu()
        {
            gameObject.SetActive(false);
        }
        #endregion
    }
}