using Codigames.Dinos.Game.UI.Popups;
using Codigames.Dinos.Game.UI.Zones;
using Codigames.Dinos.Game.UI.ZoomTests;
using UnityEngine;
using Zenject;

namespace Codigames.Dinos.Game.UI
{
    public class UIGameInstaller : MonoInstaller
    {
        #region Fields
        [SerializeField] protected ZoomSettingsMenu _zoomSettingsMenu;
        [SerializeField] protected MessagePopup _messagePopup;
        [SerializeField] protected DinoZoneMenu _dinoZoneMenu;
        #endregion

        #region Methods
        public override void InstallBindings()
        {
            Container.Bind<UISystem>().AsSingle();

            Container.BindInstance(_zoomSettingsMenu).AsSingle();
            Container.BindInstance(_messagePopup).AsSingle();
            Container.BindInstance(_dinoZoneMenu).AsSingle();
        }
        #endregion
    }
}