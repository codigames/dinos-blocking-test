using Codigames.CameraManager.Primary;
using Codigames.Dinos.Game.Units;
using Codigames.Dinos.Game.Zones;
using Codigames.Modules.CollectionUtils;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Codigames.Dinos.Game.UI.Zones
{
    public class DinoZoneMenu : MenuBase
    {
        [Inject] protected UnitsSystem _unitsSystem;
        [Inject] protected ICameraService _cameraService;

        [Space]
        [SerializeField]
        protected TextMeshProUGUI _titleLabel;
        [SerializeField]
        protected TextMeshProUGUI _levelLabel;
        [SerializeField]
        protected TextMeshProUGUI _capacityLabel;

        [Space]
        [SerializeField]
        protected Button _levelUpButton;
        [SerializeField]
        protected Button _resetButton;

        [Space]
        [SerializeField]
        protected List<DinoZoneDinosaurWidget> _dinosaurWidgets;

        protected DinoZoneController _zone;

        public void ShowMenu(DinoZoneController zone)
        {
            _zone = zone;

            _titleLabel.text = _zone.dynamicId;

            foreach (var widget in _dinosaurWidgets)
            {
                widget.Initialize(_zone, TryAddDino, TryRemoveDino);
            }

            _levelUpButton.onClick.RemoveAllListeners();
            _levelUpButton.onClick.AddListener(TryLevelUp);

            _resetButton.onClick.RemoveAllListeners();
            _resetButton.onClick.AddListener(TryReset);

            RefreshData();

            //FollowDinosaur();

            ShowMenu();
        }

        protected void FollowDinosaur()
        {
            var dinos = _zone.FindDinosaurs(any => true);
            if (dinos.Count == 0) return;

            UnitController dino = dinos.GetRandom();

            _cameraService.LockOnElement(dino.gameObject, - Vector3.up * 3.5f);
            _cameraService.ZoomTo(0.05f, 0.3f);
        }

        protected void RefreshData()
        {
            foreach (var widget in _dinosaurWidgets)
            {
                widget.RefreshLabel();
            }

            _levelLabel.text = "Level: " + _zone.Level;
            _capacityLabel.text = "Capacity: " + _zone.Capacity;
        }

        public void TryAddDino(string dinoId)
        {
            if (_zone.IsFull) return;

            var dino = _unitsSystem.CreateUnit(dinoId);
            _zone.AddDinosaur(dino);
        }

        public void TryRemoveDino(string dinoId)
        {
            var dinos = _zone.FindDinosaurs(any => any.type == dinoId);
            if (dinos.Count == 0) return;

            _zone.RemoveDinosaur(dinos[0]);
        }

        public void TryLevelUp()
        {
            _zone.LevelUp();

            RefreshData();
        }

        public void TryReset()
        {
            _zone.Reset();

            RefreshData();
        }

        public override void HideMenu()
        {
            base.HideMenu();

            //_cameraService.Unlock();
            //_cameraService.ZoomTo(0.8f, 0.3f);
        }
    }
}