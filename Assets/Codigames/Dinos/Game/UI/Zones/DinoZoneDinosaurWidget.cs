using Codigames.Dinos.Game.Zones;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Codigames.Dinos.Game.UI.Zones
{
    public class DinoZoneDinosaurWidget : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        protected string _dinoType;

        [Space]
        [SerializeField]
        protected Button _addDinoButton;
        [SerializeField]
        protected Button _removeDinoButton;
        [SerializeField]
        protected TextMeshProUGUI _currentDinosLabel;
        [SerializeField]
        protected TextMeshProUGUI _dinoTypeLabel;

        System.Action<string> _onAddDino;
        System.Action<string> _onRemoveDino;

        protected DinoZoneController _zone;
        #endregion

        #region Methods
        public void Initialize(DinoZoneController zone, System.Action<string> onAddDino, System.Action<string> onRemoveDino)
        {
            _dinoTypeLabel.text = _dinoType;

            _onAddDino = onAddDino;
            _onRemoveDino = onRemoveDino;
            _zone = zone;

            _addDinoButton.onClick.RemoveAllListeners();
            _addDinoButton.onClick.AddListener(AddDino);

            _removeDinoButton.onClick.RemoveAllListeners();
            _removeDinoButton.onClick.AddListener(RemoveDino);
        }

        public void Dispose()
        {
            _onAddDino = null;
            _onRemoveDino = null;
            _zone = null;
        }

        public void AddDino()
        {
            _onAddDino?.Invoke(_dinoType);

            RefreshLabel();
        }

        public void RemoveDino()
        {
            _onRemoveDino?.Invoke(_dinoType);

            RefreshLabel();
        }

        public void RefreshLabel()
        {
            _currentDinosLabel.text = "" + _zone.FindDinosaurs(any => any.type == _dinoType).Count;
        }
        #endregion
    }
}