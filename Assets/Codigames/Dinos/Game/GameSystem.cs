using Codigames.CameraManager.Primary;
using Codigames.Dinos.Game.Select;
using Codigames.Dinos.Game.UI;
using Codigames.Dinos.Game.Units;
using Codigames.Dinos.Game.Visitors;
using Codigames.Dinos.Game.Zones;
using Codigames.Dinos.Zones;
using UnityEngine;
using Zenject;

namespace Codigames.Dinos.Game
{
    public class GameSystem : MonoBehaviour
    {
        #region Fields
        [Inject] protected ICameraService _cameraService;
        [Inject] protected UnitsSystem _unitsSystem;
        [Inject] protected VisitorsSystem _visitorsSystem;
        [Inject] protected UISystem _uiSystem;
        [Inject] protected SelectSystem _selectSystem;

        [Inject] protected IZonesSystem _zonesSystem;
        [Inject] protected ZoneSceneLoader _zoneSceneLoader;

        protected bool _isInitialized;
        #endregion

        #region Methods
        public void Initialize()
        {
            if (_isInitialized) return;

            _cameraService.Initialize();
            _unitsSystem.Initialize();
            _visitorsSystem.Initialize();
            _selectSystem.Initialize();
            _zonesSystem.Initialize();

            _uiSystem.Initialize();

            _isInitialized = true;

            _unitsSystem.InitializeSceneUnits();
            _zoneSceneLoader.LoadZonesInScene();
        }

        public void Dispose()
        {
            if (!_isInitialized) return;

            _uiSystem.Dispose();

            _zonesSystem.Dispose();
            _selectSystem.Dispose();
            _visitorsSystem.Dispose();
            _unitsSystem.Dispose();
            _cameraService.Dispose();

            _isInitialized = false;
        }

        public void Tick(float deltaTime)
        {
            if (!_isInitialized) return;

            _cameraService.Tick(deltaTime);
            _unitsSystem.Tick(deltaTime);
            _visitorsSystem.Tick(deltaTime);

            _uiSystem.Tick(deltaTime);
        }

        private void Update()
        {
            Tick(Time.deltaTime);
        }
        #endregion
    }
}